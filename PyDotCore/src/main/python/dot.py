"""
This file is part of PyDotUml.

PyDotUml is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyDotUml is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyDotUml.  If not, see <http://www.gnu.org/licenses/>.
"""

class AbstractGraphObject:

    """Abstract base class for anything dot-related"""
    
    def __init__(self, color = "black", fontcolor = "black", fontname = "Arial",
                 fontsize = 14, label = "Unnamed AbstractGraphObject",
                 bgcolor = "white"):
        self.color = color
        self.fontcolor = fontcolor
        self.fontname = fontname
        self.fontsize = fontsize
        self.label = label
        self.bgcolor = bgcolor
    
    def toString(self, sep = ", ", end = ""):
        r"""
        @param sep: field separator string, e.g. ";\n", default ", "
        @param end: end terminator string, e.g. ";", default ""
        """
        retval = "color=" + quote(self.color) + sep
        retval += "bgcolor=" + quote(self.bgcolor) + sep
        retval += "fontcolor=" + quote(self.fontcolor) + sep
        retval += "fontname=" + quote(self.fontname) + sep
        retval += "fontsize=" + str(self.fontsize) + sep
        retval += "label=" + quote(self.label) + end
        return retval
    
    def render(self):
        """Output the object as DOT markup.  If the object's ouputfile
        property has been set then output will be written to the specified
        file.  Otherwise output will be written to the standard output
        stream.
        @todo: implement output to file."""
        print self.toString()


class AbstractGraph(AbstractGraphObject):

    """Abstract base class for Digraphs, Subgraphs, etc"""
    
    def __init__(self, color = "black", fontcolor = "black", fontname = "Arial",
                 fontsize = 14, label = "Unnamed AbstractGraph",
                 bgcolor = "white", outputfile = "dot-Digraph.png",
                 rankdir = "TB", ranksep = 0.75, nodesep = 0.25,
                 center = False):
        AbstractGraphObject.__init__(self, color, fontcolor, fontname, fontsize,
                                     label, bgcolor)
        self.outputfile = outputfile
        self.rankdir = rankdir
        self.ranksep = ranksep
        self.nodesep = nodesep
        self.center = center
        self.nodectr = 1
        self.edgectr = 1
        self.nodes = []
        self.edges = []
    
    def addNode(self, node):
        node.name = "node" + str(self.nodectr)
        self.nodectr += 1
        self.nodes.append(node)
        
    def addEdge(self, edge):
        edge.name = "edge" + str(self.edgectr)
        self.edgectr += 1
        self.edges.append(edge)
            

class Digraph(AbstractGraph):
    
    """A directed graph"""
    
    def __init__(self, color = "black", fontcolor = "black", fontname = "Arial",
                 fontsize = 14, label = "Unnamed directed graph",
                 bgcolor = "white", outputfile = "dot-Digraph.png",
                 rankdir = "TB", ranksep = 0.75, nodesep = 0.25,
                 center = False):
        AbstractGraph.__init__(self, color, fontcolor, fontname, fontsize,
                               label, bgcolor, outputfile, rankdir, ranksep,
                               nodesep, center)
        self.subgraphctr = 1        
        self.subgraphs = []
        
    def addSubgraph(self, subgraph):
        subgraph.name = "cluster" + str(self.subgraphctr)
        self.subgraphctr += 1
        self.subgraphs.append(subgraph)
        
    def toString(self):
        retval = "digraph G {\n"
        retval += AbstractGraphObject.toString(self, ";\n", "")
        if self.center:
            retval += "center=true;\n"
        else:
            retval += "center=false;\n"
        retval += "nodesep=" + str(self.nodesep) + ";\n"
        retval += "rankdir=" + self.rankdir + ";\n"
        retval += "ranksep=" + str(self.ranksep) + ";\n"
        retval += "\n"
        
        # render subgraphs
        for subgraph in self.subgraphs:
            retval += subgraph.toString() + "\n"
        
        # render nodes
        for node in self.nodes:
            retval += node.toString() + "\n"
        
        # render edges
        for edge in self.edges:
            retval += edge.toString() + "\n"
        
        retval += "}"
        
        return retval
    
    
class Subgraph(AbstractGraph):
    
    """A subgraph"""
    
    def __init__(self, graph, color = "black", fontcolor = "black",
                 fontname = "Arial", fontsize = 14,
                 label = "Unnamed subgraph", bgcolor = "white",
                 rankdir = "TB", ranksep = 0.75, nodesep = 0.25,
                 center = False, labelloc = "b"):
        AbstractGraph.__init__(self, color, fontcolor, fontname, fontsize,
                               label, bgcolor, "", rankdir, ranksep, nodesep,
                               center)
        self.labelloc = labelloc
        graph.addSubgraph(self)
    
    def addNode(self, node):
        node.name = "node" + str(self.nodectr) + self.name
        self.nodectr += 1
        self.nodes.append(node)
        
    def addEdge(self, edge):
        edge.name = "edge" + str(self.edgectr) + self.name
        self.edgectr += 1
        self.edges.append(edge)
        
    def toString(self):
        retval = "subgraph " + self.name + " {\n"
        retval += "labelloc=" + quote(self.labelloc) + ";\n"
        retval += AbstractGraphObject.toString(self, ";\n", "\n")

        # render nodes
        for node in self.nodes:
            retval += node.toString() + "\n"
        
        # render edges
        for edge in self.edges:
            retval += edge.toString() + "\n"
        
        retval += "}\n"

        return retval


class Node(AbstractGraphObject):
    
    """A node"""
    
    def __init__(self, graph, color = "black", fontcolor = "black",
                 fontname = "Arial", fontsize = 14,
                 label = "Unnamed node",  bgcolor = "white", shape = "ellipse",
                 shapefile = "", peripheries = 1):
        AbstractGraphObject.__init__(self, color, fontcolor, fontname, fontsize,
                                     label, bgcolor)
        self.shape = shape
        self.shapefile = shapefile
        self.peripheries = peripheries
        graph.addNode(self)
    
    def toString(self):
        retval = self.name + " ["
        retval += AbstractGraphObject.toString(self, ", ", ", ")
        if self.shapefile <> "":
            retval += "shapefile=" + quote(self.shapefile) + ", "
        else:
            retval += "shape=" + quote(self.shape) + ", "
        retval += "peripheries=" + str(self.peripheries)
        retval += "];"

        return retval
    
    
class Edge(AbstractGraphObject):
    
    """An edge"""

    def __init__(self, graph, headnode, tailnode, color = "black",
                 fontcolor = "black", fontname = "Arial", fontsize = 14,
                 label = "",  bgcolor = "white",
                 arrowsize = 1.0, arrowhead = "normal", arrowtail = "normal",
                 headlabel = "", taillabel = "", labelfontcolor = "black",
                 labelfontname = "Arial", labelfontsize = 14, weight = 1,
                 style = ""):
        AbstractGraphObject.__init__(self, color, fontcolor, fontname, fontsize,
                                     label, bgcolor)
        self.headnode = headnode
        self.tailnode = tailnode
        self.arrowsize = arrowsize
        self.arrowhead = arrowhead
        self.arrowtail = arrowtail
        self.headlabel = headlabel
        self.taillabel = taillabel
        self.labelfontcolor = labelfontcolor
        self.labelfontname = labelfontname
        self.labelfontsize = labelfontsize
        self.weight = weight
        self.style = style
        graph.addEdge(self)

    def toString(self):
        retval = self.headnode.name + " -> " + self.tailnode.name + " ["
        retval += AbstractGraphObject.toString(self, ", ", ", ")
        if self.style <> "":
            retval += "style=" + quote(self.style) + ", "
        retval += "arrowhead=" + quote(self.arrowhead) + ", "
        retval += "arrowtail=" + quote(self.arrowtail) + ", "
        retval += "arrowsize=" + str(self.arrowsize) + ", "
        retval += "headlabel=" + quote(self.headlabel) + ", "
        retval += "taillabel=" + quote(self.taillabel) + ", "
        retval += "labelfontcolor=" + quote(self.labelfontcolor) + ", "
        retval += "labelfontname=" + quote(self.labelfontname) + ", "
        retval += "labelfontsize=" + str(self.labelfontsize) + ", "
        retval += "weight=" + str(self.weight)
        retval += "];"
        return retval


def quote(string):
    """Return a quoted string"""
    return "\"" + str(string) + "\"" 


def main():
    graph = Digraph(outputfile = "test.dot", color = "red", fontcolor = "red",
                    fontname = "Kristen ITC", fontsize = 20,
                    label = "My first graph", center = True, nodesep = 1.0,
                    rankdir = "LR", ranksep = 1.0)
    
    node1 = Node(graph, color = "green", fontcolor = "green", 
                 fontname = "Kristen ITC", fontsize = 16,
                 label = "Node 1's label", shape = "triangle")
    
    node2 = Node(graph, color = "blue", fontcolor = "blue",
                 fontname = "Kristen ITC", fontsize = 16, shape = "hexagon")
    
    edge = Edge(graph, node1, node2, arrowhead = "onormal", arrowtail = "odot")
    
    ## RENDER (goes last):
    graph.render()
    
if __name__ == '__main__':
    main()
        
        