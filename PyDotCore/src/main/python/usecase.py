"""
This file is part of PyDotUml.

PyDotUml is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyDotUml is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyDotUml.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
01234567890123456789012345678901234567890123456789012345678901234567890123456789
"""

from dot import *

class UseCaseDiagram(Digraph):
    
    """A use case diagram"""
    
    def __init__(self, color = "black", fontcolor = "black", fontname = "Arial",
                 fontsize = 14, label = "A use case diagram", bgcolor = "white",
                 outputfile = "dot-UseCaseDiagram.png", rankdir = "TB",
                 ranksep = 0.75, nodesep = 0.25, center = False):
        Digraph.__init__(self, color, fontcolor, fontname, fontsize, label,
                         bgcolor, outputfile, rankdir, ranksep, nodesep, center)

class Actor(Subgraph):
    
    """An actor"""

    label="An unnamed actor"
    
    def __init__(self, graph, color = "black", fontcolor = "black",
                 fontname = "Arial", fontsize = 14, label = "An unnamed actor", 
                 ranksep = 0.75, nodesep = 0.25, center = False):
        Subgraph.__init__(self, graph, color, fontcolor, fontname, fontsize,
                          label, "white", "TB", ranksep, nodesep, center, "b")
    
    def attachUseCase(self, UseCase):
        pass
    
    def toString(self):
        self.color = "white"
        node = Node(self)
        node.label = ""
        node.shapefile = "../icons/actor_icon.png"
        node.color = "white"
        node.bgcolor = "white"
        return Subgraph.toString(self)
        

class UseCase(Node):
    
    """A use case"""
    
    def __init__(self, graph, color = "black", fontcolor = "black",
                 fontname = "Arial", fontsize = 14,
                 label = "An unnamed use case",  bgcolor = "white",
                 peripheries = 1, labelminlength = 9, labelwrapratio = 4):
        label = self.wrap(label, labelminlength, labelwrapratio)
        Node.__init__(self, graph, color, fontcolor, fontname, fontsize, label,
                      bgcolor, "ellipse", "", peripheries)

    def wrap(self, label, labelminlength, labelwrapratio):
        """
        Wrap label over multiple lines.
        @param label: the use case's text
        @param labelminlength: the minimum length per line before wrapping
        @param labelwrapratio: length / row count
        """
        labellength = len(label)
        words = label.split()
        wordlength = len(words)
        label = ""
        linectr = 0
        wordctr = 0
        if labellength / labelwrapratio > labelminlength:
            labelminlength = labellength / labelwrapratio
        for word in words:
            wordctr += 1
            label += word
            linectr += len(word)
            if wordctr < len(words):
                if linectr >= labelminlength:
                    label += "\\n"
                    linectr = 0
                else:
                    label += " "
        return label


class Include(Edge):
    
    """An include link"""

    def __init__(self, graph, headnode, tailnode, color = "slategray",
                 fontcolor = "black", fontname = "Arial", fontsize = 14,
                 bgcolor = "white", arrowsize = 1.0, headlabel = "",
                 taillabel = "", labelfontcolor = "black",
                 labelfontname = "Arial", labelfontsize = 14, weight = 1):
        Edge.__init__(self, graph, headnode, tailnode, color, fontcolor,
                      fontname, fontsize, "&laquo; includes &raquo;", bgcolor,
                      arrowsize, "vee", "none", headlabel, taillabel,
                      labelfontcolor, labelfontname, labelfontsize, weight,
                      "dashed")
    

class Extends(Edge):
    
    """An extends link"""
    
    def __init__(self, graph, headnode, tailnode, color = "slategray",
                 fontcolor = "black", fontname = "Arial", fontsize = 14,
                 bgcolor = "white", arrowsize = 1.0, headlabel = "",
                 taillabel = "", labelfontcolor = "black",
                 labelfontname = "Arial", labelfontsize = 14, weight = 1):
        Edge.__init__(self, graph, headnode, tailnode, color, fontcolor,
                      fontname, fontsize, "&laquo; extends &raquo;", bgcolor,
                      arrowsize, "vee", "none", headlabel, taillabel,
                      labelfontcolor, labelfontname, labelfontsize, weight,
                      "dashed")
    
    
class ActorLink(Edge):
    
    """A actor to use case link"""
    
    def __init__(self, graph, headnode, tailnode, color = "slategray",
                 fontcolor = "black", fontname = "Arial", fontsize = 14,
                 bgcolor = "white", arrowsize = 1.0, headlabel = "",
                 taillabel = "", labelfontcolor = "black",
                 labelfontname = "Arial", labelfontsize = 14, weight = 1):
        Edge.__init__(self, graph, headnode, tailnode, color, fontcolor,
                      fontname, fontsize, "", bgcolor, arrowsize,
                      "none", "none", headlabel, taillabel,
                      labelfontcolor, labelfontname, labelfontsize, weight,
                      "solid")
    
    def toString(self):
        retval = (self.headnode.nodes.__getitem__(0).name + " -> "
                  + self.tailnode.name + " [")
        retval += AbstractGraphObject.toString(self) + ", "
        if self.style <> "":
            retval += "style=" + quote(self.style) + ", "
        retval += "arrowhead=" + quote(self.arrowhead) + ", "
        retval += "arrowtail=" + quote(self.arrowtail) + ", "
        retval += "arrowsize=" + str(self.arrowsize) + ", "
        retval += "headlabel=" + quote(self.headlabel) + ", "
        retval += "taillabel=" + quote(self.taillabel) + ", "
        retval += "labelfontcolor=" + quote(self.labelfontcolor) + ", "
        retval += "labelfontname=" + quote(self.labelfontname) + ", "
        retval += "labelfontsize=" + str(self.labelfontsize) + ", "
        retval += "weight=" + str(self.weight)
        retval += "];"
        return retval


def main():
    usecasediagram = UseCaseDiagram()
    usecasediagram.outputfile = "test-use-case.dot"
    usecasediagram.color = "black"
    usecasediagram.fontcolor = "black"
    usecasediagram.fontname = "Arial"
    usecasediagram.fontsize = 20
    usecasediagram.label = "Test Use Case Diagram"
    usecasediagram.center = True
    usecasediagram.nodesep = 1.0
    usecasediagram.rankdir = "LR"
    usecasediagram.ranksep = 1.0
    
    user = Actor(usecasediagram)
    
    usecase1 = UseCase(usecasediagram, label = "Basic use case")
    
    usecase2 = UseCase(usecasediagram, label = "Included use case")
    
    usecase3 = UseCase(usecasediagram, label = "Extending use case")
    
    usecase4 = UseCase(usecasediagram, label = "This is a use case with an unfeasibly long label with many many words in it")
    
    actorlink = ActorLink(usecasediagram, user, usecase1)
    include = Include(usecasediagram, usecase1, usecase2)
    extends = Extends(usecasediagram, usecase1, usecase3)
    
    usecasediagram.render()
    

if __name__ == '__main__':
    main()
    