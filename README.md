# PyDot

A Python project from 2009, combining [Graphviz](https://graphviz.org/)'s `dot` language with Python. The idea was to create a domain-specific language to create UML diagrams.

# Overview

These days I'd probably just use [PlantUML](https://plantuml.com/), but back in 2009 all I could find was [UMLGraph](https://www.spinellis.gr/umlgraph/), which used a Java-like syntax and didn't (from memory) cover some of the (non-standard UML) diagrams I wanted to use - like [Robustness Diagrams](http://www.agilemodeling.com/artifacts/robustnessDiagram.htm).

Incidentally, I'm pretty sure I borrowed the PyDot UML icons from that Agile modelling page. Even back then I liked using hand-drawn images and text for UML modelling.

The scripts seem to be Windows batch files. One of these days I might see about updating them to `sh` scripts.

# PlantUML example

```plantuml
@startuml

left to right direction

actor Applicant
actor Registrar
actor Student
boundary UI13 as "UI13 University Application Form"
boundary Create as "Create Student icon"
control EnrollUniversity as "Enroll in university"
"UC 17 Enroll in Seminar" as (EnrollSeminar)

Applicant --> UI13
Registrar --> UI13
Registrar --> Create
Registrar --> EnrollSeminar
Student --> EnrollSeminar
Create --> EnrollUniversity

@enduml
```

![](plantuml-example.png)

*(Snippet from [the original](http://www.agilemodeling.com/artifacts/robustnessDiagram.htm))*

# License

Copyright (c) 2009-2021 John Niven

Licensed under the GPL, v3.
